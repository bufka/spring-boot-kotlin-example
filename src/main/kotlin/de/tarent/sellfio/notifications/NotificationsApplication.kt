package de.tarent.sellfio.notifications

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class NotificationsApplication

fun main(args: Array<String>) {
    SpringApplication.run(NotificationsApplication::class.java, *args)
}
